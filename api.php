<?php
/*
предположим, что мы имеем две таблицы - темы и вебинары

TABLE webinar_themes
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(128)

TABLE webinars
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	theme_id INTEGER NOT NULL,
	webinar_name VARCHAR(128),
	webinar_date datetime
*/

// собираемся отправить json на любой другой домен

header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
header('Cache-Control: no-cache, must-revalidate, max-age=0');

// здесь можно добавить базовую авторизацию

////////////////////////////////////////////////////////////////////////////////////////

// класс, который формирует часть строки запроса вида 
// IN (1,2,3)
// если на входе пустой параметр, то вернется пустая строка, которая не сломает запрос

class SqlIN
{	
    public static function create($value)	
    {
		$paramsSQL = "";		
		if (implode(",", $value) != ""){	
			$paramsSQL = "IN ($params)";	
		}
		return $paramsSQL;
    }	
}

/////////////////////////////////////////////////////////////////////////////////////////

// пример данных
// $input = '{"theme":[1,2,3],"month":[9]}';

// получаем данные POST запросом
$input = file_get_contents("php://input");
$data = json_decode($input, true);

// создаем куски запроса MySQL c помощью статического класса SqlIN
$themes = SqlIN::create($data['theme']);
$months = SqlIN::create($data['month']);

// начинаем формировать ответ
$response = [];
$index = 0;

// спрашиваем базу с использованием функции MONTH
include_once ("db.php");

$sql = "SELECT
	webinar_themes.id,	
	webinar_themes.name,
	webinars.id,
	webinars.theme_id,
	webinars.webinar_name,
	webinars.webinar_date
FROM webinar_themes
JOIN webinars ON webinar_themes.id = webinars.theme_id
WHERE webinar_themes.id {$themes} AND MONTH(webinars.webinar_date) {$months}
ORDER BY webinar_themes.id";
	
$result = $mysqli->query($sql, MYSQLI_STORE_RESULT);
while ($row = $result->fetch_object()){	

	$response[$index] = array(	
		'id' => $row->id,
		'webinar_themes' => $row->name,
		'webinar_name' => $row->webinar_name,
		'webinar_date' => $row->webinar_date,
	);
	$index++;	
		
}

// сформировали ответ и отдали 

$mysqli->close();
echo json_encode($response);
 
